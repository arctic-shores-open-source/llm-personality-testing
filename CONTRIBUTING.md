# CONTRIBUTING

Thank you for your interest in this project!

## I want to ask a question

Please email [open-source@arcticshores.com](mailto:open-source@arcticshores.com) and our team will put you in touch with the developer(s) responsible for this project.

## I want to discuss an idea / file a bug / suggest an enhancement

Please [file an issue](https://gitlab.com/arctic-shores-open-source/llm-personality-testing/-/issues/new) to start a discussion.

## I want to provide a patch / pull request

We warmly welcome code contributions! If you would like to contribute code, please submit a pull request.

This project enforces the Developer Certificate of Origin (DCO) on Pull Requests (PRs). This means that all commit messages must contain a signature line to indicate that the developer accepts the DCO.

The DCO is a lightweight way for contributors to certify that they wrote (or otherwise have the right to submit) the code and changes they are contributing to the project. Here is the full [text of the DCO](https://developercertificate.org/), reformatted for readability:

    By making a contribution to this project, I certify that:

      (a) The contribution was created in whole or in part by me and I have the right to submit it under the open source license indicated in the file; or

      (b) The contribution is based upon previous work that, to the best of my knowledge, is covered under an
          appropriate open source license and I have the right under that license to submit that work with modifications, whether created in whole or in part by me, under the same open source license (unless I am permitted to submit under a different license), as indicated in the file; or

      (c) The contribution was provided directly to me by some other person who certified (a), (b) or (c) and I have not modified it.

      (d) I understand and agree that this project and the contribution are public and that a record of the contribution
          (including all personal information I submit with it, including my sign-off) is maintained indefinitely and may be redistributed consistent with this project or the open source license(s) involved.


Contributors indicate that they adhere to these requirements by addinga `Signed-off-by` line to their commit messages.  For example:

    This is my commit message

    Signed-off-by: Random J Developer <random@developer.example.org>

Ways to Sign Commits
--------------------

1. Signing each commit manually

   Of course, when you are making a commit and editing the commit
   message, you can sign it manually, by typing the line above but
   with your GitHub account's name and email address.  However, this
   has the obvious downsides of being tedious and error-prone.


2. Using ``git -s``

   Git has a ``-s | --signoff`` command-line flag that will
   automatically add your 'Signed-off-by' line to your commit message.
   For an interactive commit like the following:

   ```bash
    git commit -s
    ```

   you should see the 'Signed-off-by' line in your editor when it brings up the buffer representing the commit message.  If you supply the commit message on the command-line, the 'Signed-off-by' line will be automatically added for you.

   ```bash
   git commit -s -m "This is my commit message"
    ```
   This approach has the advantage of being fairly straightforward and requiring no configuration, but the downside that you need to remember to use the flag with each commit.

Troubleshooting DCOs
--------------------

If you have authored a commit that is missing its 'Signed-off-by'
line, you can amend your commits and push them to GitHub.
```bash
git commit --amend --signoff
```

If you've pushed your changes to Gitlab already you'll need to force push your branch after this with ``git push -f``.

If your Pull Request fails the DCO check, it will be necessary to fix
the entire commit history for the PR. Best practice is to squash the
commit history to a single commit, append the DCO sign-off as
described above, and force push. For example, if you have 2 commits in
your history (Note the ~2):

```bash
git rebase -i HEAD~2
(interactive squash + DCO append)
git push origin -f
```
Note that, in general, rewriting history in this way may introduce
issues to the review process and this should only be done to correct a
DCO mistake.

## Code of Conduct

Contributors are kindly asked to read and follow our [Code of Conduct](CODE_OF_CONDUCT.md).
