Automated Personality Testing of LLMs
====================================================

Overview
--------

The Automated Personality Testing of LLMs repository originally conceived of by Jasper Wolf offers a comprehensive toolkit for evaluating the personality traits of large language models, using standardized personality inventories. In this case implemented with OpenAI's GPT-3.5 and GPT-4 models. This project aims to explore and quantify the personality dimensions of these models, providing insights into their behavioral tendencies and characteristics.

The report for which the repository was originally created can be found here: [ChatGPT vs Personality Assessments: Does it have the right personality traits to get an interview?](https://www.arcticshores.com/insights/chatgpt-vs-personality-assessments-does-it-have-the-right-personality-traits-to-get-an-interview)

Stay tuned for our journal article/pre-print!

Features
--------

*   **Testing using Personality Inventories**: Leverages the IPIP NEO-60 personality inventory to ChatGPT's scores on the Big 5 Personality Dimensions (OCEAN). Easily modifiable for different personality inventories, as long as they are presented in the same format as specified in data/raw/ipip_neo_60_updated_randomised.csv
*   **Customizable Model Interaction**: Supports various configurations for model interaction, including model version, temperature settings, and Prompt Style.
*   **Extensive Data Collection**: Facilitates the collection and storage of model responses, the ability to run experiments multiple times, and compute scores for each trial.

Repository Structure
--------------------

*   `item_testing_and_scoring.ipynb`: Main Jupyter notebook for running tests and analyzing results.
*   `main.py`: Core of the cli functionality (see instructions below)
*   `client.py`: Defines the `ChatGPTClient` for interacting with the ChatGPT API.
*   `config.py`: Contains configuration settings for the project.
*   `prompts.py`: Manages prompt creation for the personality test.
*   `utils.py`: Includes utility functions such as `run_multiple_trials`, `generate_personality_responses` and `calculate_mean_and_sd_for_runs`.


Getting Started
---------------

1. **Installation**

We'll need to set up a virtual environment into which to install the project dependencies:

```bash
python3 -m venv venv
```

Now we activate it:

```bash
source venv/bin/activate
```

And install the dependencies:

```bash
python3 -m pip install -r requirements.txt
```

*This exact way of installing requirements.txt helps to ensure that the python-dotenv package installs correctly, allowing you to reference your OPENAI_API_KEY in a .env file. See the next step for more detail.*

 *Also note that the creation of the `venv` and installation of dependencies is a one-time deal. If you want to revisit the project (say, after restarting your computer), you just need to run `source venv/bin/activate` again to reactivate your virtual environment.*

2.  **Setting your OPENAI API KEY**: The way that this repository references the `OPENAI_API_KEY` is through a local `.env` file in the root folder of the repository, which contains the API key and then explicitly uses that API key in the Python code. That way the API key is accessible in both the main.py script and any associated jupyter notebooks.
    > If you don't have an OpenAI API key yet, you can create one [here](https://platform.openai.com/account/api-keys)

    - Once you have your API key, go to the project folder you want to create the .env file in.

    - Once you create the .env file using the terminal or an integrated development environment (IDE), copy your secret API key and set it as the OPENAI_API_KEY in your .env file.

    - The .env file should look like the following:

        ```bash
        # Once you add your API key below, make sure to not share it with anyone! The API key should remain private.

        OPENAI_API_KEY=sk-abc123
        ```

3.  **Running Tests**: Use `item_testing_and_scoring.ipynb` to run personality tests. Configure the notebook to specify the model, temperature, and prompt style parameters (i.e. direction, dimension).

4.  **Data Analysis**: Analyze the responses and calculate summary statistics like mean score and standard deviation.


 Running `main.py`
-------------

The `main.py` script is a command-line interface tool designed to automate the process of personality testing of large language models (LLMs) like OpenAI's GPT-3.5 and GPT-4.

### Running the Script:

The script can be executed from the command line with customizable parameters for flexible testing.

Run `main.py` from the command line using the following format:

```bash
python main.py --data_path <path_to_data> --model <model_name> --temperature <temperature_value> --direction <test_direction> --dimension <personality_dimension> --n_trials <number_of_trials> --output_path <output_directory>
```

#### Command-Line Arguments:

- `--data_path`: Specifies the path to the CSV file containing the personality test items.
- `--model`: Defines the ChatGPT model version to test (e.g., 'gpt-3.5-turbo').
- `--temperature`: Sets the temperature for the model's responses.
- `--direction`: Determines the direction for personality testing (e.g., 'Maximise', 'Minimise', 'Neutral').
- `--dimension`: Chooses the personality dimension to test (e.g., 'Agreeableness').
- `--n_trials`: Indicates the number of trials to run for each test.
- `--output_path`: Specifies the directory path where the output files will be saved.

An example command with the parameters correctly inputted would be:

```bash
python main.py --data_path "data/raw/ipip_neo_60_updated_randomised.csv" --model "gpt-4" --temperature 0.7 --direction "Minimise" --dimension "Agreeableness" --n_trials 1 --output_path "outputs"
```

### Script Output

Calling main.py produces two types of outputs:

1. **Full Detailed Responses**: Stored in the specified output directory, containing responses for each test run, in a file called "full_responses_df_{model}_{temperature}_{direction}_{dimension}_{time}.csv".
2. **Summary Statistics**: Includes mean and standard deviation, which are also saved in the output directory in a file called "summary_stats_{model}_{temperature}_{direction}_{dimension}_{time}.csv".

Example Jupyter Notebook
-------------

To get a better understanding of the data, what each parameter controls, and how to run the main functions in the repo, you can refer to the example notebook called [item_testing_and_scoring.ipynb](example_notebooks/item_testing_and_scoring.ipynb)

### Running the notebook
1. To reference your OPENAI API Key in the jupyter notebook, use the `python-dotenv` package, referencing it at the top of your jupyter notebook as:
```python
%load_ext dotenv
%dotenv
```
> in order to check if this is working correctly, you can run `os.getenv('OPENAI_API_KEY')` in a new cell to check that the environment variable with your API key is accessible in the notebook.

2.  Initialize parameters for a test run in the Jupyter notebook.

    ```python
    MODEL = "gpt-3.5-turbo"
    TEMPERATURE = 0.0
    DIRECTION = "Maximise"
    DIMENSION = "Agreeableness"
    ```

3.  Using those parameters, run one or multiple trials and collect responses in a dataframe.

    ```python
    all_responses_test = run_multiple_trials(
        data_path="../data/raw/ipip_neo_60_updated_randomised.csv",
        n_trials=3,
        model = MODEL,
        temperature = TEMPERATURE,
        direction = DIRECTION,
        dimension=DIMENSION)
    ```

4.  Analyze the personality scores of the model responses according to the Big 5 framework and save the results.



    ```python
    result_df_updated = calculate_mean_and_sd_for_runs(all_responses_test)

    write_dataframe_to_csv(
        df=result_df_updated,
        output_path="../outputs/summary_outputs",
        results_stage="NEO_60_summary_stats",
        model=MODEL,
        temperature=TEMPERATURE,
        direction=DIRECTION,
        dimension=DIMENSION,
        run_timestamp=RUN_TIMESTAMP
        )
    ```


Potential Future Research Directions
------------------------------------

*   **More Personality Inventories**: Extend testing to include other personality inventories.
*   **More LLMs**: Evaluate other large language models for comparative analysis.
*   **More Analytics and Statistical Analyses**: Run more analytics on the model responses and their personality scores (e.g. correlation, internal consistency, etc.)
*   **Prompt Variation**: Experiment with different styles of prompts, such as job description analysis.

Contributing
------------

Contributions are welcome! If you're interested in enhancing the functionality or extending the scope of this project, please read our contribution guidelines.

License
-------

This project is open source and available under the [Apache 2.0 License](LICENSE.txt).

* * *
