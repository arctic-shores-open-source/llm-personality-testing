import argparse
import logging
import os
import sys
import time

# Importing necessary custom modules
from src.utils import (
    calculate_mean_and_sd_for_runs,
    run_multiple_trials,
    write_dataframe_to_csv,
)

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(message)s",
)


def main(data_path, model, temperature, direction, dimension, n_trials, output_path):
    # Reading the dataset
    # Handle exceptions if file not found
    try:
        os.path.isfile(data_path)
    except Exception as e:
        logging.error(f"Failed to find data file: {e}")
        sys.exit(1)

    # Run parameters
    run_timestamp = time.strftime("%Y%m%d-%H%M%S")

    # Running the trials
    all_responses = run_multiple_trials(
        data_path=data_path,
        n_trials=n_trials,
        model=model,
        temperature=temperature,
        direction=direction,
        dimension=dimension,
    )

    # define output paths
    full_output_path = os.path.join(output_path, "full_outputs")
    summary_output_path = os.path.join(output_path, "summary_outputs")

    # Save full output
    write_dataframe_to_csv(
        df=all_responses,
        output_path=full_output_path,
        results_stage="full_responses_df",
        model=model,
        temperature=temperature,
        direction=direction,
        dimension=dimension,
        run_timestamp=run_timestamp,
    )

    # Calculate and save summary output
    result_df_updated = calculate_mean_and_sd_for_runs(all_responses)
    write_dataframe_to_csv(
        df=result_df_updated,
        output_path=summary_output_path,
        results_stage="summary_stats",
        model=model,
        temperature=temperature,
        direction=direction,
        dimension=dimension,
        run_timestamp=run_timestamp,
    )

    # Optionally displaying the first few rows of the result
    print(result_df_updated.head())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run personality tests on ChatGPT models.",
    )
    parser.add_argument(
        "--data_path",
        type=str,
        default="data/raw/ipip_neo_60_updated_randomised.csv",
        help="path to personality items.csv",
    )
    parser.add_argument(
        "--model",
        type=str,
        default="gpt-3.5-turbo",
        help="Model to use for the test",
    )
    parser.add_argument(
        "--temperature",
        type=float,
        default=0.0,
        help="Temperature setting for the model",
    )
    parser.add_argument(
        "--direction",
        type=str,
        default="Neutral",
        help="prompt to Maximise/Minimise personality dimension,\
              or Neutral for baseline",
    )
    parser.add_argument(
        "--dimension",
        type=str,
        default="Agreeableness",
        help="Personality dimension to test",
    )
    parser.add_argument(
        "--n_trials",
        type=int,
        default=1,
        help="Number of trials to run",
    )
    parser.add_argument(
        "--output_path",
        type=str,
        default="outputs",
        help="Path to save output files",
    )

    args = parser.parse_args()

    main(
        args.data_path,
        args.model,
        args.temperature,
        args.direction,
        args.dimension,
        args.n_trials,
        args.output_path,
    )
