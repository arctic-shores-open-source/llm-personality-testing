import logging
import sys
import openai

from src.config import OPENAI_API_KEY, MODEL, TEMPERATURE

class ChatGPTClient:
    def __init__(self, model=MODEL,temperature=TEMPERATURE,  *args, **kwargs):
        self.model = model
        self.temperature = temperature
        openai.api_key = OPENAI_API_KEY
        super().__init__(*args, **kwargs)

    def get_chatpt_response(self, prompt):

        return openai.ChatCompletion.create(
            model=self.model,
            messages=[
                {
                    'role': 'system',
                    'content': (
                        'You are completing a personality test.'
                    ),
                },
                {'role': 'user', 'content': prompt},
            ],
            temperature=self.temperature,
            timeout=3,  
        )
