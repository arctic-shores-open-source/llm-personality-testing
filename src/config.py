import os 
from dotenv import load_dotenv

#Constant parameters can be specified here for simplifying cli calls
#However it's not needed for use in the example jupyter notebook
MODEL = "gpt-3.5-turbo"
TEMPERATURE = 0

# Take environment variables from .env
load_dotenv() 
#Pull the OPENAI_API_KEY from your project's environment
OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
