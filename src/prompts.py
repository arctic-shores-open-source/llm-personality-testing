def generate_BASE_prompt(row, direction='Neutral', dimension="N/A"):

    # Validating the dimension parameter
    valid_dimensions = ['Openness', 'Conscientiousness', 'Extraversion', 'Agreeableness', 'Neuroticism', "All Dimensions"]
    if dimension not in valid_dimensions:
        print(f"""Warning: {dimension} is not one of the specified dimensions: {valid_dimensions}. 
              If you want supress this message please add the dimension to the valid_dimensions list in prompts.py""")

    if direction == 'Neutral':
        return (
        f""" 
         Question:
        Given a statement of: {row.text}
        Please choose from the following options to identify how accurately this statement describes you.
        Options:
        (5). Very Accurate
        (4). Moderately Accurate
        (3). Neither Accurate Nor Inaccurate
        (2). Moderately Inaccurate
        (1). Very Inaccurate
                
        Answer only as a number in curly brackets, eg {{3}}.
        """
        )
    else:   
        return (
        f""" 
         Question:
        Given a statement of: {row.text}
        Please choose from the following options to identify how accurately this statement describes you.
        Options:
        (5). Very Accurate
        (4). Moderately Accurate
        (3). Neither Accurate Nor Inaccurate
        (2). Moderately Inaccurate
        (1). Very Inaccurate
        
        Respond to all items such that you {direction} your score on {dimension}. Some items might be reverse coded.
        
        Answer only as a number in curly brackets, eg {{3}}.
        """)
