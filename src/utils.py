import re
import time

import logging

import numpy as np
import pandas as pd

from src.client import ChatGPTClient
from src.prompts import generate_BASE_prompt

# Function to extract the answer
def extract_answer(text):
    '''Extract the numeric value of the answer from ChatGPTs response'''
    if isinstance(text, (str, bytes)):
        match = re.search(r'(\d+)', text)
        return float(match.group(1)) if match else None
    else:
        return None

def transform_answer(row, score):
    '''Convert the answer to the corresponding Likert scale score (assuming it's between 1 to 5)
    i.e. converting to numeric and reverse coding it if the key for the item is -1'''

    numeric_score = int(score)

    # Apply reverse scoring transformation if needed
    if row.key == -1:
        likert_score = 6 - numeric_score
    elif row.key == 1: 
        likert_score = numeric_score
    else:
        raise ValueError(f'Key {row.key} is not in the correct format')
    print(likert_score)
    return likert_score


def calculate_mean_and_sd_for_runs(df):
    '''
    Calculates the total score, mean, and standard deviation for each OCEAN personality 
    dimension (Openness, Conscientiousness, Extraversion, Agreeableness, Neuroticism) 
    for each test run in the provided DataFrame.

    The function iterates over each run, filtering the DataFrame for each OCEAN dimension, 
    and then computes the total score, mean, and standard deviation for that dimension 
    in that specific run. The results are returned in a format where each row represents 
    a unique combination of run, OCEAN dimension, and the calculated statistics.

    Parameters:
    df (DataFrame): A pandas DataFrame containing the processed LLM responses. It should 
    have columns for OCEAN labels and recoded answers for each run (e.g., 'recoded_answer_1', 
    'recoded_answer_2', etc.).

    Returns:
    DataFrame: A pandas DataFrame with columns 'Trial', 'Label_Ocean', 'Total_Score', 'Mean', 
    and 'Std'. Each row corresponds to a unique OCEAN dimension and run combination, with 
    the total score, mean, and standard deviation for that combination.
    '''
    # List of OCEAN dimensions
    ocean_dimensions = ['O', 'C', 'E', 'A', 'N']
    # Extract columns for recoded answers
    answer_cols = [col for col in df.columns if col.startswith('recoded_answer_')]
    # Calculate the maximum number of trials (runs) in the dataframe
    number_of_trials = max([int(re.search('(\d+)$', col).group(1)) for col in answer_cols])

    # List to store results
    results_list = []

    # Iterate over each test run
    for i in range(1, number_of_trials+1):
        # Filtering the DataFrame for each run
        run_df = df[['label_ocean', f'recoded_answer_{i}']]

        # Iterating over each OCEAN dimension
        for dimension in ocean_dimensions:
            # Filtering the DataFrame for the current dimension
            dimension_df = run_df[run_df['label_ocean'] == dimension]
            # Calculating sum, mean, and standard deviation for the current dimension
            sum_scores = dimension_df[f'recoded_answer_{i}'].sum(skipna=True)
            mean_scores = dimension_df[f'recoded_answer_{i}'].mean(skipna=True)
            std_scores = dimension_df[f'recoded_answer_{i}'].std(skipna=True)

            # Append results to the list
            results_list.append({
                'Trial': f'Trial_{i}',
                'Label_Ocean': dimension,
                'Total_Score': sum_scores,
                'Mean': mean_scores,
                'Std': std_scores
            })

    # Convert list to a DataFrame
    result_df = pd.DataFrame(results_list)

    return result_df

def generate_personality_responses(client, df, trial, direction='Neutral', dimension='N/A'):

    # Initialize a new DataFrame to store the responses
    # Initialize a new DataFrame to store the responses
    responses_df = df[['text','label_ocean','key']].copy()
    responses_df['prompt'], responses_df['response'], responses_df['trial'] = [np.nan, np.nan, np.nan]


    # Iterate over each row in the DataFrame
    # just doing one row here to test
    for index, row in responses_df.iterrows():
        #generate prompt for current item
        prompt = generate_BASE_prompt(row,  direction, dimension)
        
        #create list to handle timeout errors
        failed_indices = []
        
        #TODO make this triggered only when verbose output is presented?
        print("Item Number:{}".format(index))
        
        try:
            #start timer
            start_time = time.time()
            
            # Send the prompt to ChatGPT API
            response = client.get_chatpt_response(prompt)
        
            end_time = time.time()
        
            #TODO set this to verbose only output?
            # print level time    
            print(f"\nTime taken: {round(end_time - start_time,3)} seconds")

            # Extract the response from the API output
            response_content = response["choices"][0]["message"]["content"]

            # Append the prompt to the new DataFrame
            responses_df.at[index, "prompt"] = prompt

            # Append the response to the new DataFrame
            responses_df.at[index, "response"] = response_content

            # Append the trial number to the new DataFrame
            responses_df.at[index, 'trial'] = trial
            # Extract the final answer from the response and append it to new DataFrame
            responses_df.at[index, "extracted_answer"] = extract_answer(response_content)

            # Extract the final answer from the response and append it to new DataFrame
            responses_df.at[index, "recoded_answer"] = transform_answer(row, responses_df.at[index, "extracted_answer"])
        except Exception as e:
                print(f"Failed at index {index} with error: {e}")
                failed_indices.append(index)
    
    # Retry for failed indices
    for index in failed_indices:
        row = responses_df.iloc[index]
        prompt = row['prompt']
        try:
            print(f"Retrying for index {index}")
                #start timer
            start_time = time.time()
            
            # Send the prompt to ChatGPT API
            response = client.get_chatpt_response(prompt)
        
            end_time = time.time()
        
            #TODO set this to verbose only output?
            # print level time    
            print(f"\nTime taken: {round(end_time - start_time,3)} seconds")

            # Extract the response from the API output
            response_content = response["choices"][0]["message"]["content"]

            # Append the prompt to the new DataFrame
            responses_df.at[index, "prompt"] = prompt

            # Append the response to the new DataFrame
            responses_df.at[index, "response"] = response_content

            # Append the trial number to the new DataFrame
            responses_df.at[index, 'trial'] = trial
            # Extract the final answer from the response and append it to new DataFrame
            responses_df.at[index, "extracted_answer"] = extract_answer(response_content)

            # Extract the final answer from the response and append it to new DataFrame
            responses_df.at[index, "recoded_answer"] = transform_answer(row, responses_df.at[index, "extracted_answer"])
        except Exception as e:
            print(f"Retry failed at index {index} with error: {e}")
    return responses_df

def run_multiple_trials(data_path="../data/raw/ipip_neo_60_updated_randomised.csv", 
                        n_trials=3, 
                        model = "gpt-3.5-turbo",
                        temperature = 0.0,
                        direction='Neutral', 
                        dimension='N/A'):
    '''Run generate_personality_responses() multiple times and save the responses in one dataframe in order to evaluate the consistency of the responses
    Parameters: 
        data_path (str): the path to the csv file containing the items from the personality inventory
        
        n_trials (int): the number of different trials to run
        
    Returns:
        all_responses_df: a dataframe of the responses for each item in the inventory, with a new column for each trial
        '''
    """
    Executes the generate_personality_responses() function multiple times to collect
    and evaluate the consistency of responses across different trials. The function 
    reads a personality inventory from a CSV file, runs the trials, and aggregates 
    all responses in a single DataFrame.


    Parameters:
    data_path (str): Path to the CSV file containing the items from the personality inventory.
                     Default path is "../data/raw/ipip_neo_60_updated_randomised.csv".
    n_trials (int): The number of trials to run. Each trial generates a new set of responses.
                    Default is 3 trials.
    model (str): The model version for the ChatGPTClient. Defaults to "gpt-3.5-turbo".
    temperature (float): The temperature setting for response generation. Defaults to 0.0.
    direction (str): Specifies the direction of the responses. The options are 'Maximise',
                     'Minimise', or 'Neutral', or some variation thereof. Default is 'Neutral'.
    dimension (str): Specifies the dimension of the personality inventory to focus on. Default is 'N/A',
                     which means no specific dimension is targeted.

    Returns:
    DataFrame: A pandas DataFrame containing the original inventory items ('text', 'label_ocean', 'key')
               and the responses for each item across all trials. Each trial's responses are added
               as new columns with suffixes indicating the trial number.
    """
    
    df = pd.read_csv(data_path)

    # Initialize a DataFrame to store all responses
    all_responses_df = df[['text','label_ocean','key']].copy()
    
    client = ChatGPTClient(model, temperature)
    # Apply the function the number of times specified by n_trials
    for trial in range(1, n_trials+1):
        responses_df = generate_personality_responses(client, df, trial, direction, dimension)
        #extract just prompt and responses
        responses_df_filter = responses_df[['prompt', 'response', "extracted_answer", "recoded_answer"]]
        #recode with trial number
        responses_df_filter = responses_df_filter.add_suffix("_"+str(trial))
    
        all_responses_df = pd.concat([all_responses_df, responses_df_filter], axis = 1)

    return all_responses_df

def write_dataframe_to_csv(df,
                           results_stage, 
                           model, 
                           temperature, 
                           direction,
                           dimension,
                           run_timestamp, 
                           output_path="outputs"):
    df.to_csv(
        "{output_path}/{results_stage}_{model}_{temperature}_{direction}_{dimension}_{time}.csv"
        .format(
            output_path=output_path,
            results_stage=results_stage,
            model=model,
            temperature=temperature, 
            dimension=dimension, 
            direction=direction,
            time=run_timestamp,
        )
    )
