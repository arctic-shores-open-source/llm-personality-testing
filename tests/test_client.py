import pytest
from unittest.mock import patch, MagicMock
from src.client import ChatGPTClient
from src.config import MODEL, TEMPERATURE

class TestChatGPTClient:
    @pytest.fixture
    def client(self):
        return ChatGPTClient()

    def test_init_default_values(self, client):
        assert client.model == MODEL
        assert client.temperature == TEMPERATURE

    def test_init_custom_values(self):
        custom_model = 'custom-model'
        custom_temperature = 0.5
        client = ChatGPTClient(model=custom_model, temperature=custom_temperature)
        assert client.model == custom_model
        assert client.temperature == custom_temperature

    @patch('src.client.openai.ChatCompletion.create')
    def test_get_chatpt_response_valid(self, mock_create, client):
        mock_create.return_value = MagicMock()
        prompt = "Test prompt"
        response = client.get_chatpt_response(prompt)
        mock_create.assert_called_once()
        assert response is not None

    @patch('src.client.openai.ChatCompletion.create')
    def test_get_chatpt_response_timeout(self, mock_create, client):
        mock_create.side_effect = TimeoutError
        prompt = "Test prompt"
        with pytest.raises(TimeoutError):
            client.get_chatpt_response(prompt)

    @patch('src.client.openai.ChatCompletion.create')
    def test_get_chatpt_response_api_error(self, mock_create, client):
        mock_create.side_effect = Exception("API Error")
        prompt = "Test prompt"
        with pytest.raises(Exception) as exc_info:
            client.get_chatpt_response(prompt)
        assert "API Error" in str(exc_info.value)