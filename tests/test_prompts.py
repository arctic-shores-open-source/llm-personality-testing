import pytest
from unittest.mock import patch, MagicMock
from src.prompts import generate_BASE_prompt

class TestPrompts:
    @pytest.fixture
    def sample_row(self):
        return MagicMock(text="Sample statement")

    # Test for default parameters (Neutral direction)
    def test_generate_base_prompt_default(self, sample_row):
        expected_prompt = (
            f""" 
         Question:
        Given a statement of: {sample_row.text}
        Please choose from the following options to identify how accurately this statement describes you.
        Options:
        (5). Very Accurate
        (4). Moderately Accurate
        (3). Neither Accurate Nor Inaccurate
        (2). Moderately Inaccurate
        (1). Very Inaccurate
                
        Answer only as a number in curly brackets, eg {{3}}.
        """
        ).replace("\n", "") #differences in new line indicators were causing failures

        actual_prompt = generate_BASE_prompt(sample_row).replace("\n", "")

        assert actual_prompt == expected_prompt

    # Test for valid non-default parameters
    def test_generate_base_prompt_valid_params(self, sample_row):
        direction = 'Maximise'
        dimension = 'Openness'
        expected_prompt = (
            f""" 
         Question:
        Given a statement of: {sample_row.text}
        Please choose from the following options to identify how accurately this statement describes you.
        Options:
        (5). Very Accurate
        (4). Moderately Accurate
        (3). Neither Accurate Nor Inaccurate
        (2). Moderately Inaccurate
        (1). Very Inaccurate
        
        Respond to all items such that you {direction} your score on {dimension}. Some items might be reverse coded.
        
        Answer only as a number in curly brackets, eg {{3}}.
        """
        ).replace("\n", "") #differences in new line indicators were causing failures
        actual_prompt = generate_BASE_prompt(sample_row, direction=direction, dimension=dimension).replace("\n", "")
        assert actual_prompt == expected_prompt
    # Test for invalid dimension parameter
    @patch('builtins.print')
    def test_generate_base_prompt_invalid_dimension(self, mock_print, sample_row):
        invalid_dimension = 'InvalidDimension'
        generate_BASE_prompt(sample_row, dimension=invalid_dimension)
        mock_print.assert_called()
