import pytest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from src.utils import extract_answer, transform_answer, calculate_mean_and_sd_for_runs, generate_personality_responses, run_multiple_trials, write_dataframe_to_csv

class TestUtils:

    # Tests for extract_answer
    def test_extract_answer_with_number(self):
        assert extract_answer("The answer is 42") == 42

    def test_extract_answer_without_number(self):
        assert extract_answer("No number here") is None

    def test_extract_answer_non_string(self):
        assert extract_answer(1234) is None

    # Tests for transform_answer
    def test_transform_answer_reverse_scoring(self):
        row = MagicMock(key=-1)
        assert transform_answer(row, 4) == 2

    def test_transform_answer_no_reverse_scoring(self):
        row = MagicMock(key=1)
        assert transform_answer(row, 3) == 3

    def test_transform_answer_invalid_key(self):
        row = MagicMock(key=0)
        with pytest.raises(ValueError):
            transform_answer(row, 3)

# Tests for calculate_mean_and_sd_for_runs
    def test_calculate_mean_and_sd_for_runs_valid_data(self):
        data = {
            'label_ocean': ['O', 'C', 'E', 'A', 'N'],
            'recoded_answer_1': [3, 4, 5, 2, 1],
            'recoded_answer_2': [4, 3, 2, 5, 4]
        }
        df = pd.DataFrame(data)
        result = calculate_mean_and_sd_for_runs(df)
        assert len(result) == 10  # 5 dimensions * 2 trials
        assert all(col in result.columns for col in ['Trial', 'Label_Ocean', 'Total_Score', 'Mean', 'Std'])

    def test_calculate_mean_and_sd_for_runs_missing_values(self):
        data = {
            'label_ocean': ['O', 'C', 'E', 'A', 'N'],
            'recoded_answer_1': [3, np.nan, 5, 2, 1],
            'recoded_answer_2': [4, 3, np.nan, 5, 4]
        }
        df = pd.DataFrame(data)
        result = calculate_mean_and_sd_for_runs(df)
        assert len(result) == 10

    # Tests for generate_personality_responses
    @patch('src.utils.ChatGPTClient')
    def test_generate_personality_responses(self, mock_client):
        mock_client_instance = MagicMock()
        mock_client_instance.get_chatpt_response.return_value = {"choices": [{"message": {"content": "Response 5"}}]}
        mock_client.return_value = mock_client_instance

        data = {'text': ['Q1'], 'label_ocean': ['O'], 'key': [1]}
        df = pd.DataFrame(data)
        result = generate_personality_responses(mock_client_instance, df, 1)
        assert 'prompt' in result.columns
        assert 'response' in result.columns
        assert 'trial' in result.columns

    # Tests for run_multiple_trials
    @patch('src.utils.generate_personality_responses')
    @patch('src.utils.pd.read_csv')
    def test_run_multiple_trials(self, mock_read_csv, mock_generate_responses):
        # Mock the pd.read_csv function to return a sample DataFrame
        mock_read_csv.return_value = pd.DataFrame({'text': ['Q1'], 'label_ocean': ['O'], 'key': [1]})

        # Mock the generate_personality_responses function to return a sample DataFrame
        mock_generate_responses.return_value = pd.DataFrame({
            'prompt': ['prompt_1'],
            'response': ['response_1'],
            'extracted_answer': [5],
            'recoded_answer': [5]
        })

        # Run the function under test
        result = run_multiple_trials(data_path='path/to/test.csv', n_trials=2)

        # Assert that the DataFrame contains the expected columns
        expected_columns = ['text', 'label_ocean', 'key', 'prompt_1', 'response_1', 'extracted_answer_1', 'recoded_answer_1']
        assert all(column in result.columns for column in expected_columns)

    # Tests for write_dataframe_to_csv
    @patch('src.utils.pd.DataFrame.to_csv')
    def test_write_dataframe_to_csv(self, mock_to_csv):
        df = pd.DataFrame({'col1': [1, 2], 'col2': [3, 4]})
        write_dataframe_to_csv(df, 'test_stage', 'test_model', 0.7, 'Neutral', 'N', '123456', 'output')
        expected_filename = 'output/test_stage_test_model_0.7_Neutral_N_123456.csv'
        mock_to_csv.assert_called_with(expected_filename)
